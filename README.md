# Boom

Boom is a tool that makes working with MicroPython-based boards easier.

It supports interfacing with ESP32-based boards over a serial connection.

![Boom main menu](docs/boom_main.png)

Tested on Windows, but should be cross-platform.

Features:
* Simple GUI interface
* Connect over Serial without typing in the port name
* Compile your files to .mpy files automatically (requires mpy-cross.exe in your PATH)
* Compile & upload with one click (requires mpfshell in your PATH)
* Easy two-step board flashing (requires esptool.py/esptool.py.exe in your PATH)
* Simple compile/upload configuration using an mpycfile

## Getting started

To start using Boom, you'll need some prerequisites. The following instructions
will guide you through installing Python and our toolchain on Windows.
If you're on Mac or other Unix OS, sorry, I assume you can figure it out for yourself.


### Install Python
First, you'll need to install Python 3.

https://www.python.org/downloads/

Click on the Download Python button. You should be given the most current version by default.

Open the installer.

You'll need to add Python to your PATH.
Check this box to ensure Python gets added to your path.
![Install Python, add Python to PATH](docs/python_path.png)

If you already have a Python 3.6 or newer Python installed, try
opening a command line `run cmd` and trying `python --version`.
If you get an error, ask Google how to add Python to your path.

Once you've got Python installed, either restart your computer
or run `refreshenv` in cmd.exe

Finally, run `pip install esptool mpy-cross` to install our toolchain.

Note: mpy-cross version matters, since the compiled python
version type must match that of the firmware.
You can specify which mpy version to install: `pip install mpy-cross==1.18`

See https://docs.micropython.org/en/latest/reference/mpyfiles.html for more details.

### Using Boom

Place Boom.exe in your project folder, alongside your .py source files.

Then, create an mpycfile (covered in next section).

Connect your device via a USB Serial interface, then open Boom.
You should see your device show up in the menu.
Double-click on an entry to attempt to connect to it. Make sure
no other software is accessing the same serial port.

To install MicroPython on your board, first click `Erase Flash`
and wait for the confirmation. Then, click `Flash MicroPython`
and wait. If you see "hash verified", you've done it!

Finally, click Compile & Upload to upload your files.

You only need to perform the erasing and flashing step once unless
you erase your board's flash again.

## mpycfile

Boom uses a file called `mpycfile` to determine what files get uploaded and how.

The file format is not the same as makefile. It is much simpler.

Each line should contain a relative path to a file, and optionally a single-character prefix.

` ` No prefix will upload the file directly to the board.<br/>
`@` This denotes the path to the esp32 firmware file that is used to flash your board.<br/>
`$` The file will be compiled and uploaded as an .mpy, and files matching the file stem (i.e. `stem` of `stem.py`) will be deleted from the board.<br/>

For example, a project folder might have the following structure:

```
| project/
|   boom.exe
|   mpycfile
|   main.py
|   two.py
|   esp32_v1.18.bin
|
.   src/
.   |   driver.py
.   |   test.py
|
```

and the following mpycfile:

```
@./esp32_v1.18.bin
main.py
./two.py
$src/driver.py
./src/test.py
```

which would use esp32_v1.18.bin as the MicroPython firmware file for flashing,
compile and upload `driver.py`,
and verbatim upload `main.py`, `two.py`, and `test.py`.


By default, Boom assumes that the `mpycfile` is in the same directory as Boom.exe


# Screenshots

![Boom port select menu](docs/boom_main.png)


![Boom board flash and upload menu](docs/boom_boardmenu.png)


![Boom flash output dialog](docs/boom_flashsuccess.png)

# Troubleshooting

## ValueError: incompatible .mpy file

If you see `ValueError: incompatible .mpy file` in the REPL, your
mpy-cross version is not compatible with your MicroPython firmware version.

Try `pip install --upgrade mpy-cross`
or, if you're running an older firmware, you can specify the version number.
`pip install mpy-cross==1.18`
