""" Modified INI-style config parser

Includes a parser for my weird glob-like wildcard destination syntax.
Given the path of a source file, this is useful for specifying where to
copy that file to without having to type out the whole path.
Syntax is not stable yet.


changes include:
Section names are internally lowercased
Section names in the .ini file do not matter
Duplicate section and option names do not raise an error (strict = False)
In certain sections, `:` and `=` are forced to be key/value delimiters
In certain sections, bare options are allowed
The contents of each section are added to a separate list `commands`
"""
import sys
import re
import configparser
from configparser import (
    DuplicateSectionError, DuplicateOptionError, MissingSectionHeaderError, NoSectionError
)
from configparser import SectionProxy
import glob
import pathlib
from pathlib import PurePosixPath
from os.path import isfile

# allow things like `c:/` in option name, even with `:` as a delimiter
configparser.RawConfigParser._OPT_NV_TMPL = r"""
    ^(?P<option>(?:\s*[a-zA-Z]:[/\\])?.*?)\s*(?:(?P<vi>{delim})\s*(?P<value>.*))?$
"""

# Regex match a wildcard anchor
""" examples of intended result
source          dp              interpolated (path to copy to)
/foo/bar/x.py   /*foo/*         /foo/bar/x.py
/foo/bar/dog/*  /*bar/*         /bar/dog/*
/foo/bar/dog/*  *bar/*          /dog/*
"""
ANCHORRE = re.compile(r"""
    (?P<sl>\\*|/*)?  # optionally match preceding slashes
    (?P<wp>\*?)  # match preceding wildcard symbol
    (?P<anchor>[^\*\t\r\0\\/]+)  # match path anchor name
    (?P<ws>\*?)  # match proceding wildcard symbol
    (?:\\*|/*)?.*  # match end slash
""")


class MPYCParser(configparser.ConfigParser):
    def __init__(self, defaults=None):
        super().__init__(self,
            strict=False,
            allow_no_value=True,
            inline_comment_prefixes='#',
            default_section="VARS",
        )
        self.optionxform = lambda option: option
        self.commands = []

    # The following function has been slightly modified from
    # https://github.com/python/cpython/blob/3.10/Lib/configparser.py
    def _read(self, fp, fpname):
        """Parse a sectioned configuration file.
        Each section in a configuration file contains a header, indicated by
        a name in square brackets (`[]'), plus key/value options, indicated by
        `name' and `value' delimited with a specific substring (`=' or `:' by
        default).
        Values can span multiple lines, as long as they are indented deeper
        than the first line of the value. Depending on the parser's mode, blank
        lines may be treated as parts of multiline values or ignored.
        Configuration files may include comments, prefixed by specific
        characters (`#' and `;' by default). Comments may appear on their own
        in an otherwise empty line or may be entered in lines holding values or
        section names. Please note that comments get stripped off when reading configuration files.
        """
        elements_added = set()
        cursect = None                        # None, or a dictionary
        curcmd = -1
        sectname = None
        optname = None
        lineno = 0
        indent_level = 0
        e = None                              # None, or an exception
        for lineno, line in enumerate(fp, start=1):
            comment_start = sys.maxsize
            # strip inline comments
            inline_prefixes = {p: -1 for p in self._inline_comment_prefixes}
            while comment_start == sys.maxsize and inline_prefixes:
                next_prefixes = {}
                for prefix, index in inline_prefixes.items():
                    index = line.find(prefix, index + 1)
                    if index == -1:
                        continue
                    next_prefixes[prefix] = index
                    if index == 0 or (index > 0 and line[index - 1].isspace()):
                        comment_start = min(comment_start, index)
                inline_prefixes = next_prefixes
            # strip full line comments
            for prefix in self._comment_prefixes:
                if line.strip().startswith(prefix):
                    comment_start = 0
                    break
            if comment_start == sys.maxsize:
                comment_start = None
            value = line[:comment_start].strip()
            if not value:
                if self._empty_lines_in_values:
                    # add empty line to the value, but only if there was no
                    # comment on the line
                    if (
                        comment_start is None
                        and cursect is not None
                        and optname
                        and cursect[optname] is not None
                    ):
                        cursect[optname].append('')  # newlines added at join
                else:
                    # empty line marks end of value
                    indent_level = sys.maxsize
                continue
            # continuation line?
            first_nonspace = self.NONSPACECRE.search(line)
            cur_indent_level = first_nonspace.start() if first_nonspace else 0
            if (cursect is not None and optname
                    and cur_indent_level > indent_level):
                cursect[optname].append(value)
            # a section header or option header?
            else:
                indent_level = cur_indent_level
                # is it a section header?
                mo = self.SECTCRE.match(value)
                if mo:
                    sectname = mo.group('header')
                    sectname = sectname.lower()
                    if sectname in self._sections:
                        if self._strict and sectname in elements_added:
                            raise DuplicateSectionError(sectname, fpname,
                                                        lineno)
                        cursect = self._sections[sectname]
                        elements_added.add(sectname)
                    elif sectname == self.default_section:
                        cursect = self._defaults
                    else:
                        cursect = self._dict()
                        self._sections[sectname] = cursect
                        self._proxies[sectname] = SectionProxy(self, sectname)
                        elements_added.add(sectname)
                    # So sections can't start with a continuation line
                    optname = None
                    self.commands.append({'cmd': sectname, 'items': []})
                    curcmd += 1

                # no section header in the file?
                elif cursect is None:
                    raise MissingSectionHeaderError(fpname, lineno, line)
                # an option line?
                else:
                    mo = self._optcre.match(value)
                    if mo:
                        optname, vi, optval = mo.group('option', 'vi', 'value')
                        if not optname:
                            e = self._handle_error(e, fpname, lineno, line)
                        optname = self.optionxform(optname.rstrip())
                        if (self._strict
                                and (sectname, optname) in elements_added):
                            raise DuplicateOptionError(sectname, optname,
                                                       fpname, lineno)
                        elements_added.add((sectname, optname))
                        # This check is fine because the OPTCRE cannot
                        # match if it would set optval to None
                        if optval is not None:
                            optval = optval.strip()
                            cursect[optname] = [optval]
                            self.commands[curcmd]['items'].append([optname, optval])
                        elif vi:
                            # empty value handling
                            cursect[optname] = ''
                            self.commands[curcmd]['items'].append([optname, ''])
                        else:
                            # valueless option handling
                            cursect[optname] = None
                            self.commands[curcmd]['items'].append([optname, None])
                    else:
                        # a non-fatal parsing error occurred. set up the
                        # exception but keep going. the exception will be
                        # raised at the end of the file and will contain a
                        # list of all bogus lines
                        e = self._handle_error(e, fpname, lineno, line)
        self._join_multiline_values()
        # if any parsing errors occurred, raise an exception
        if e:
            raise e


# returns empty result if source_glob doesn't lead to a real file
def expand_wildcards(source_glob, destination: str):
    sources = glob.glob(source_glob, recursive=True)
    sources = [p for p in sources if isfile(p)]
    result = []
    for i in sources:
        dest = expand_wildcard(i, destination)
        result.append((i, str(dest)))
    return result


# expects l&r whitespace stripped inputs. sfile should be a file, not a directory.
def expand_wildcard(sfile: str, d: str):
    d_is_dir = True if d is None or d == '' or d[-1] in '/\\' else False
    # pathlib strips trailing `/` so must check if destination is a directory
    # using the original path string
    sp = pathlib.PurePath(sfile)
    if d is None:
        d = ''
    dp = pathlib.PurePosixPath(d)
    filename = None
    if d_is_dir or dp.name == '*':
        filename = sp.name
    elif dp.stem == '*' and dp.suffix:
        filename = sp.with_suffix(dp.suffix).name
    else:
        filename = dp.name
    filename = PurePosixPath(filename)
    parts = sp.parent.parts
    wildcarded_path = path_after_anchor = PurePosixPath()
    mo = ANCHORRE.match(str(dp.parent))
    if mo:
        sl, wp, anchor, ws = mo.group('sl', 'wp', 'anchor', 'ws')
        if wp or ws:
            parts_after_anchor = parts[parts.index(anchor):]
            path_after_anchor = pathlib.PurePosixPath(*parts_after_anchor)
    # Regex match number of directory wildcards
    n_copy_parent = dp.parent.parts.count('*')  # number of levels to copy
    if n_copy_parent:
        wildcarded_parts = parts[-n_copy_parent:]
        wildcarded_path = pathlib.PurePosixPath(*wildcarded_parts)
    return path_after_anchor / wildcarded_path / filename


def main():
    print("Hello")
    cases = [
        # ("src/az.py", None),  # /az.py
        # ("src/az.py", ""),  # /az.py
        # ("src/az.py", "*"),  # /az.py
        # ("src/az.py", "/"),  # /az.py
        # ("src/az.py", "/*"),  # /az.py
        # ("src/az.py", "/*.mpy"),  # /az.mpy
        # ("src/az.py", "*.mpy"),  # /az.mpy
        # ("src/az.py", ".mpy"),  # /.mpy
        # ("src/az.py", "/*.*"),  # /az.*
        ("foo/src/*", "*/"),  # /src/az.py
        ("foo/*", "*/"),  # /src/az.py
        ("foo/src/az.py", "*/"),  # /src/az.py
        ("foo/src/az.py", "*/*"),  # az.py to /src
        ("foo/src/az.py", "*src/*"),  # /az.py
        ("foo/src/az.py", "*foo/*"),  # /az.py
        ("foo/src/az.py", "*foo/src/*"),  # /src/az.py
        ("foo/src/az.py", "/*foo/src/*"),  # /foo/src/az.py
        ("foo/src/az.py", "/*foo/*/*"),  # undefined behavior
        ("foo/src/az.py", "/src/*"),  # /src/az.py
        ("foo/src/az.py", "*/*"),  # az.py to /foo/src
        ("foo/src/az.py", "*/*.mpy"),  # compile to src/az.mpy
        ("foo/src/az.py", "**"),  # copy to /foo/src/az.py
        ("foo/src/az.py", "**.mpy"),  # compile to /foo/src/az.mpy
        ("src/", None),  # copy contents of src/ to /
        ("src/*", None),  # copy contents of src/ to /
    ]
    for i in cases:
        print(i[0], i[1], expand_wildcards(*i))
        input()


if __name__ == "__main__":
    main()

    # DONE: check for all windows drive refs e.g. C: and others and replace with sentinel character
    # TODO: .lower all section names internally
    # DONE: step-by-step parsing for procedure multiple sections with same name
    # DONE: wildcard interpolation for destination paths based on input path
    #       by default it should copy only file to root of pyboard
    #       detect .mpy in destination and mark for compilation
    #       .mpy should count as /*.mpy which should be equivalent to compile without specified path
    #       bare : or = should count as copy full path
    #       path anchors i.e. C:\user\project\src\stem.py:  src*/heck/*.py --> /heck/stem.py
    #                                                      /src*/heck/*.py --> /src/heck/stem.py
    #                                                      /project*/heck/*.py --> project/src/heck/stem.py
    # r/^\s*[a-zA-Z]:\S/
    # TODO: glob doesn't recursively expand folder hits i.e. /foo/* globs to /foo/src/, not /foo/src/file.py
