import os
import argparse
import subprocess
import serial.tools.list_ports
import tkinter as tk
from tkinter import ttk, messagebox
import warnings
import pathlib as p
import shutil
import shlex
import glob
import re
import hashlib
import mpycfile_parser
from mpfshell.mp.mpfexp import MpFileExplorer, RemoteIOError
from retry import retry
from retry.api import retry_call

parser = argparse.ArgumentParser()
parser.add_argument('-v', '--verbose', action='store_true')
parser.add_argument('-s', '--src', default=".\\src\\")
parser.add_argument('-m', '--makefile', default=".\\mpycfile")
parser.add_argument('--cache_dir', default="./__mpycache__/")
parser.add_argument('-p', '--port')
parser.add_argument('--no_gui', action='store_true')

# https://stackoverflow.com/questions/12090503/listing-available-com-ports-with-python

port = None
firmware_file = None

interrupt = None


def get_sha256_hexdigest(target):
    with open(target, 'rb') as f:
        return hashlib.sha256(f.read()).hexdigest()


def attempt_enter_repl(ser):
    ser.write(b'\x02\x03\x02')  # exit raw REPL, CTRL+C, CTRL+B again
    ser.write(b'\x04')
    ser.reset_input_buffer()
    ser.write(b'\x03\x02')
    reply = ser.read_until(b'>>>', 500).decode('latin1')
    ser.reset_input_buffer()
    return reply


class breakable(object):
    # https://stackoverflow.com/questions/11195140/break-or-exit-out-of-with-statement
    # god this is stupid why can't break work for `with`
    class Break(Exception):
        """Break out of the with statement"""

    def __init__(self, value):
        self.value = value

    def __enter__(self):
        return self.value.__enter__()

    def __exit__(self, etype, value, traceback):
        error = self.value.__exit__(etype, value, traceback)
        if etype == self.Break:
            return True
        return error


def clear_text(text, disable=False):
    text['state'] = 'normal'
    text.delete('1.0', 'end')
    if disable:
        text['state'] = 'disabled'


def gui(args):
    root = tk.Tk()
    root.title("BOOM (WTF MicroPython Utility)")
    root.columnconfigure(0, weight=1)
    root.rowconfigure(0, weight=1)
    port_select(args, root)
    root.mainloop()


def port_select(args, root):
    frame = ttk.Frame(root, padding="15 15 15 15")
    frame.grid(column=0, row=0, sticky="NEWS")
    frame.columnconfigure(0, weight=1)
    frame.rowconfigure(0, weight=1)

    tree_frame = ttk.Frame(frame)
    tree_frame.grid(column=0, row=0, sticky='news')
    tree_frame.columnconfigure(0, weight=1)
    tree_frame.rowconfigure(0, weight=1)

    tree = ttk.Treeview(tree_frame, columns=('name', 'desc', 'hwid'), show='headings')
    tree.grid(column=0, row=0, sticky='news')
    tree.heading('name', text="")
    tree.heading('desc', text="Description")
    tree.heading('hwid', text="Hardware Identifier")
    scrollbar = ttk.Scrollbar(tree_frame, orient=tk.VERTICAL, command=tree.yview)
    tree.configure(yscroll=scrollbar.set)
    scrollbar.grid(row=0, column=1, sticky='news')

    def refresh_ports(e=None):
        for i in tree.get_children():
            tree.delete(i)
        ports = get_ports(args)
        for name, desc, hwid in ports:
            item = tree.insert('', 'end', values=(name, desc, hwid), iid=name)
        if not ports:
            open_button.state(['disabled'])

    def handle_open_port(e=None):
        global port
        port = tree.focus()
        if port:
            root.unbind("<Return>")
            frame.grid_remove()
            board_menu(args, root, back)

    tree.bind("<Double-1>", handle_open_port)

    def back():
        frame.grid()
        root.bind("<Return>", handle_open_port)
        refresh_ports()

    actions = ttk.Frame(frame, padding="0 15 20 5")
    actions.grid(column=0, row=1, sticky='news')
    actions.columnconfigure(0, weight=1)
    open_button = ttk.Button(actions, text="Open", command=handle_open_port)
    open_button.grid(column=10, row=0, padx=10, sticky='e')
    open_button.state(['disabled'])
    refresh_button = ttk.Button(actions, text="Refresh", command=refresh_ports)
    refresh_button.grid(column=9, row=0, padx=10, sticky='e')
    tree.bind('<<TreeviewSelect>>', lambda x: open_button.state(['!disabled']))
    root.bind("<Return>", handle_open_port)
    refresh_ports()


def board_menu(args, root, back):
    global port, firmware_file
    frame = ttk.Frame(root, padding="15 15 15 15")
    frame.grid(column=0, row=0, sticky="NEWS")
    frame.columnconfigure(0, weight=1)
    frame.rowconfigure(2, weight=1)

    status = tk.StringVar()
    status.set(f"Connecting to {port}...")
    # bodge ttk button textvariable
    ttk.Button(frame, text="", command=lambda: status.set())
    status_label = ttk.Label(frame, textvariable=status)
    status_label.grid(column=0, row=2)
    board_frame = ttk.LabelFrame(frame, labelwidget=status_label)
    board_frame.grid(column=0, row=2, sticky='news')
    board_frame.columnconfigure(0, weight=1)
    board_frame.rowconfigure(0, weight=1)

    board_term = tk.Text(board_frame, width=60, height=8)
    board_term.grid(column=0, row=0, sticky='news')
    # board_term['state'] = 'disabled'

    def handle_back(e=None, force=True):
        frame.grid_forget()
        back()

    def handle_erase(e=None):
        print(repr(port))
        board_flash_gui(
            args, root,
            shlex.split(f'esptool.py.exe --port {str(port)} erase_flash '),
            "Erasing flash..."
        )
        upload_button.state(['!disabled'])
        clear_text(board_term, True)

    def handle_flashmp(e=None):
        if not args.makefile_exists:
            args.makefile_exists = args.config.read(args.makefile)
            if not args.makefile_exists:
                messagebox.showerror("File Not Found", f"\n\nCould not find mpycfile at {args.makefile}")
                raise FileNotFoundError("File Not Found", f"Could not find mpycfile at {args.makefile}")
        try:
            firmware_file = args.config.options("firmware")[-2]
        except (mpycfile_parser.NoSectionError, IndexError) as err:
            messagebox.showerror(
                "Firmware file not specified.",
                f"Firmware file not specified.\n{err}\n\nPlease specify the path of a valid firmware file in the mpycfile under a [firmware] section."
            )
            raise EnvironmentError("Firmware file not specified in mpycfile.")
        if not p.Path(firmware_file).exists():
            messagebox.showerror(
                "File Not Found",
                f"Could not find{firmware_file}\n\nPlease ensure that the file exists, or remove it from the mpycfile."
            )
            raise FileNotFoundError(str(firmware_file))
        board_flash_gui(
            args, root,
            [
                'esptool.py.exe',
                '--port', port,
                '--chip', 'esp32',
                '--baud', '460800',
                'write_flash',
                '-z', '0x1000',
                firmware_file
            ],
            "Flashing MicroPython...",
        )
        upload_button.state(['!disabled'])
        clear_text(board_term, True)

    def handle_upload(e=None, force=True):
        clear_text(board_term, True)
        root.update_idletasks()
        enter_repl()
        upload_button.state(['disabled'])
        root.update_idletasks()
        try:
            mpyc(args)
        except FileNotFoundError as err:
            messagebox.showerror("File Not Found", f"{err}\n\n\
                Please ensure that the file exists, or remove it from the mpycfile.")
            raise
        enter_repl()
        upload_button.state(['!disabled'])

    flash_frame = ttk.Frame(frame)
    flash_frame.grid(column=0, row=1)
    ttk.Button(flash_frame, text="Erase Flash", command=handle_erase).grid(column=0, row=0)
    ttk.Button(flash_frame, text="Flash MicroPython", command=handle_flashmp).grid(column=1, row=0)

    back_button = ttk.Button(frame, text="Back", command=handle_back)
    back_button.grid(column=0, row=0, sticky='nsw')

    upload_button = ttk.Button(board_frame, text="Compile & Upload", command=handle_upload)
    upload_button.grid(column=0, row=2, sticky='news')
    upload_button.state(['disabled'])
    root.update()

    def enter_repl():
        conn_success = False
        with breakable(serial.Serial(baudrate=115200, timeout=2)) as ser:
            ser.port = port
            try:
                ser.open()
            except serial.SerialException as e:
                messagebox.showerror("Connection Failed", f"Connection failed.\nCheck that {ser.port} is not in use.\n\n{e}")
            else:
                data = attempt_enter_repl(ser)
                board_term['state'] = 'normal'
                board_term.insert('1.0', data.strip('>\n '))
                board_term['state'] = 'disabled'
                print(data)
                if "MicroPython" in data:
                    conn_success = True

        if conn_success:
            status.set(f"Successfully entered MicroPython REPL on {port}.")
            print("Connected.\n")
            upload_button.state(['!disabled'])
        else:
            status.set("Not connected.")
            upload_button.state(['disabled'])

    enter_repl()

    board_term['state'] = 'disabled'


def board_flash_gui(args, root, cmd, text=""):
    global port
    global interrupt
    window = tk.Toplevel(root)
    window.title(f'{port}')
    frame = ttk.Frame(window, padding="30 30 30 30")
    frame.grid(column=0, row=0, sticky="NEWS")
    frame.columnconfigure(0, weight=1)
    frame.rowconfigure(0, weight=0)
    frame.rowconfigure(1, weight=1)
    title_label = tk.Label(frame, text=text)
    title_label.grid(column=0, row=0)
    console_frame = ttk.Frame(frame, padding="0 0 10 0", width=500, height=300)
    console_frame.columnconfigure(0, weight=1)
    console_frame.rowconfigure(0, weight=1)
    console_frame.grid(column=0, row=1, sticky='news')

    # console_frame['relief'] = 'sunken'
    console_text = tk.Text(console_frame, width=100, height=15)
    console_text.grid(column=0, row=0, sticky='news')
    scrollbar = ttk.Scrollbar(console_frame, orient=tk.VERTICAL, command=console_text.yview)
    console_text.configure(yscroll=scrollbar.set)
    scrollbar.grid(row=0, column=1, sticky='news')
    root.after(10)

    def set_output(output, maxlines=None):
        clear_text(console_text, True)
        out = re.sub(r'\r\n', '\n', str(output))
        out = out.split('\n')
        if maxlines:
            out = out[-maxlines:]
        console_text.insert('1.0', '\n'.join(out))
        # console_out.set('\n'.join(out))

    def handle_output(e=None, output_total=""):
        global interrupt
        if interrupt:
            return
        output = process.stdout.readline().decode('utf-8 ')
        # output_total += output
        print(output)
        console_text['state'] = 'normal'

        return_code = process.poll()
        if return_code is None:
            # set_output(output_total, 15)
            out = re.sub(r'\r\n', '\n', str(output))
            console_text.insert('end', out)
            # root.update()
            root.after(100, lambda: handle_output(output_total=output_total))
        else:
            print('RETURN CODE', return_code)
            # Process has finished, read rest of the output
            rest = ''.join([x.decode('utf-8') for x in process.stdout.readlines()])
            # output_total += rest
            print(rest)
            # set_output(output_total, 15)
            out = re.sub(r'\r\n', '\n', str(rest))
            console_text.insert('end', out)
        console_text.see(tk.END)
        console_text['state'] = 'disabled'

    clear_text(console_text, True)

    full_cmd = ' '.join(['>>>', *cmd]) + '\n'
    print(full_cmd)

    process = subprocess.Popen(cmd,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,
                               # shell=True,
                               # universal_newlines=True,
                               )
    interrupt = False
    root.after(1, lambda: handle_output(None, full_cmd))


def get_ports(args):
    ports = sorted(serial.tools.list_ports.comports())
    for port, desc, hwid in ports:
        print(f"found {port}: {desc} [{hwid}]")
    return ports


def operate_on_items(items, f):
    for src_glob, dst_glob in items:
        targets = mpycfile_parser.expand_wildcards(src_glob, dst_glob)
        for src, dst in targets:
            f(src, dst)


def mpyc(args):
    mpfx = MpFileExplorer(f"ser:{port}")
    arch = "xtensawin"

    # clear cache directory
    cache_dir = p.Path(args.cache_dir)
    # for f in glob.glob(str(cache_dir / '**')):
    #     os.remove(f)
    #     print(f"removed {f}")
    # try:
    #     os.mkdir(cache_dir)
    # except FileExistsError:
    #     pass

    def create_intermediate_cache_dirs(dst: p.Path, exist_ok=False):
        if isinstance(dst, p.PurePath):
            dst = p.Path(dst)
        dst.parent.mkdir(parents=True, exist_ok=exist_ok)

    def compile_and_cache(src, dst):
        sp = p.PurePosixPath(src)
        dp = p.PurePosixPath(dst)
        if dp.is_absolute():
            dp = dp.relative_to('/')
        dp = p.Path(dp)
        dst_fullpath = cache_dir / dp
        # print(dst_fullpath)
        # detect if file should be compiled
        create_intermediate_cache_dirs(dst_fullpath, exist_ok=True)
        if dp.suffix == ".mpy":
            # if so, compile and copy to cache dir
            print(f"compiling: {sp.name} \t for {dp}")
            # xtensawin for ESP32
            cmd = f"mpy-cross -march={arch} {src} -o {dst_fullpath}"
            stream = os.popen(cmd)
            out = stream.read()
            print(cmd, out)
        else:
            # for regular files, just copy to cache dir
            print(f"copying to cache: {sp.name}")
            shutil.copy(src, dst_fullpath)

    def upload_from_dir(folder, root=None, hash_skip=False):
        if root is None:
            root = folder
        for i in folder.iterdir():
            mpypath = i.relative_to(root)
            mpypathstr = mpypath.as_posix()
            if i.is_file():
                # mpfx.put(i.as_posix(), mpypathstr)
                if hash_skip:
                    try:
                        mpy_hash = mpfx.sha256(mpypathstr)
                    except RemoteIOError as e:
                        if 'No such file' in str(e):
                            mpy_hash = None
                        elif 'hashlib' in str(e):
                            hash_skip = False
                            warnings.warn("Remote device does not support sha256 for hash generation. Consider turning off 'Skip up-to-date files'")
                        else:
                            raise

                if not hash_skip or mpy_hash is None or mpy_hash != get_sha256_hexdigest(i):
                    print(f"uploading {i.as_posix()} to {mpypathstr}")
                    mpfx.put(i.as_posix(), mpypathstr)
                else:
                    print(f"skipping {mpypathstr} \tas it's already up-to-date")

            elif i.is_dir():
                print(f"creating {mpypathstr}")
                mpfx.mkdir(mpypathstr, exist_ok=True)
                upload_from_dir(i, root=root, hash_skip=hash_skip)

    shutil.rmtree(cache_dir)
    cache_dir.mkdir(exist_ok=True)

    for section in args.config.commands:
        if section["cmd"] == "upload":
            operate_on_items(section["items"], compile_and_cache)
            upload_from_dir(cache_dir, hash_skip=True)

    mpfx.__del__()  # explicitly close port

    # files = mpfx.ls(add_dirs=False)
    # regexs = [re.compile(f'^{re.escape(p.PurePath(i).stem)}') for i in pyc_files]

    # for f in files:
    #     for r in regexs:
    #         if r.match(f):
    #             print(f" * rm {f}")
    #             mpfx.rm(f)


def main():
    clargs = parser.parse_args()  # parse command line args
    args = clargs  # use `args` to hold both clargs and config options

    # try to read config (mpycfile)
    args.makefile_exists = False
    config = mpycfile_parser.MPYCParser()
    # config.read() returns a list of successfully read file names.
    # Can use args.makefile_exists to check if file read was successful.
    args.makefile_exists = config.read(clargs.makefile)
    args.config = config

    if not args.no_gui:
        gui(args)

    print("Exiting...")


if __name__ == "__main__":
    main()


# TODO: add file time comparison, only upload if newer option
# TODO: option moshing. args should take precedence over config file option
# TODO: globb match files on board for rm
# TODO: remove files command
# TODO: refresh button on upload page
# TODO: closing flashing window should either cancel process or not be allowed
# TODO: give feedback for when erasing/flashing/uploading is done
# TODO: show files on board
# TODO: log file
# TODO: update documentation
# TODO: pyqt
# TODO: terminal colors in flash/upload output to make errors more visible
